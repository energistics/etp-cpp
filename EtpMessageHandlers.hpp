#ifndef ETP_MESSAGE_HANDLERS_HPP_
#define ETP_MESSAGE_HANDLERS_HPP_

#include "EtpErrorCodes.hpp"
#include "BaseHandler.hpp"

using namespace Etp;

namespace Energistics {
namespace Protocol {
namespace ChannelDataFrame {

    /// Basic handler for protocol: 2 (ChannelDataFrame), role: producer
    class Producer : public BaseHandler
    {
    public:    
        Producer()
            : BaseHandler(2, "producer")
        {        
            m_messages_handled.insert(1);
        }        
        virtual ~Producer() {}
        
        /// Handle websocket messages for protocol: 2 (ChannelDataFrame), role: producer
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 1 :            
                SigRequestChannelData(header, decode<RequestChannelData>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the RequestChannelData signal
        etp_sig_conn_t add_RequestChannelData_handler(const etp_slot_t<RequestChannelData>  &subscriber) { return SigRequestChannelData.connect(subscriber); }
        
        // Message senders
        /// Send the ChannelDataFrameSet on the websocket
        int64_t send_ChannelDataFrameSet(ChannelDataFrameSetPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChannelDataFrameSet(ChannelDataFrameSet&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the ChannelMetadata on the websocket
        int64_t send_ChannelMetadata(ChannelMetadataPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChannelMetadata(ChannelMetadata&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<RequestChannelData> SigRequestChannelData;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace ChannelDataFrame {

    /// Basic handler for protocol: 2 (ChannelDataFrame), role: consumer
    class Consumer : public BaseHandler
    {
    public:    
        Consumer()
            : BaseHandler(2, "consumer")
        {        
            m_messages_handled.insert(4);
            m_messages_handled.insert(3);
        }        
        virtual ~Consumer() {}
        
        /// Handle websocket messages for protocol: 2 (ChannelDataFrame), role: consumer
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 4 :            
                SigChannelDataFrameSet(header, decode<ChannelDataFrameSet>(decoder));
                break;
            case 3 :            
                SigChannelMetadata(header, decode<ChannelMetadata>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the ChannelDataFrameSet signal
        etp_sig_conn_t add_ChannelDataFrameSet_handler(const etp_slot_t<ChannelDataFrameSet>  &subscriber) { return SigChannelDataFrameSet.connect(subscriber); }
        /// connect to the ChannelMetadata signal
        etp_sig_conn_t add_ChannelMetadata_handler(const etp_slot_t<ChannelMetadata>  &subscriber) { return SigChannelMetadata.connect(subscriber); }
        
        // Message senders
        /// Send the RequestChannelData on the websocket
        int64_t send_RequestChannelData(RequestChannelDataPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_RequestChannelData(RequestChannelData&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<ChannelDataFrameSet> SigChannelDataFrameSet;
        etp_signal_t<ChannelMetadata> SigChannelMetadata;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace ChannelStreaming {

    /// Basic handler for protocol: 1 (ChannelStreaming), role: producer
    class Producer : public BaseHandler
    {
    public:    
        Producer()
            : BaseHandler(1, "producer")
        {        
            m_messages_handled.insert(1);
            m_messages_handled.insert(9);
            m_messages_handled.insert(4);
            m_messages_handled.insert(5);
            m_messages_handled.insert(0);
        }        
        virtual ~Producer() {}
        
        /// Handle websocket messages for protocol: 1 (ChannelStreaming), role: producer
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 1 :            
                SigChannelDescribe(header, decode<ChannelDescribe>(decoder));
                break;
            case 9 :            
                SigChannelRangeRequest(header, decode<ChannelRangeRequest>(decoder));
                break;
            case 4 :            
                SigChannelStreamingStart(header, decode<ChannelStreamingStart>(decoder));
                break;
            case 5 :            
                SigChannelStreamingStop(header, decode<ChannelStreamingStop>(decoder));
                break;
            case 0 :            
                SigStart(header, decode<Start>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the ChannelDescribe signal
        etp_sig_conn_t add_ChannelDescribe_handler(const etp_slot_t<ChannelDescribe>  &subscriber) { return SigChannelDescribe.connect(subscriber); }
        /// connect to the ChannelRangeRequest signal
        etp_sig_conn_t add_ChannelRangeRequest_handler(const etp_slot_t<ChannelRangeRequest>  &subscriber) { return SigChannelRangeRequest.connect(subscriber); }
        /// connect to the ChannelStreamingStart signal
        etp_sig_conn_t add_ChannelStreamingStart_handler(const etp_slot_t<ChannelStreamingStart>  &subscriber) { return SigChannelStreamingStart.connect(subscriber); }
        /// connect to the ChannelStreamingStop signal
        etp_sig_conn_t add_ChannelStreamingStop_handler(const etp_slot_t<ChannelStreamingStop>  &subscriber) { return SigChannelStreamingStop.connect(subscriber); }
        /// connect to the Start signal
        etp_sig_conn_t add_Start_handler(const etp_slot_t<Start>  &subscriber) { return SigStart.connect(subscriber); }
        
        // Message senders
        /// Send the ChannelData on the websocket
        int64_t send_ChannelData(ChannelDataPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChannelData(ChannelData&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the ChannelDataChange on the websocket
        int64_t send_ChannelDataChange(ChannelDataChangePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChannelDataChange(ChannelDataChange&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the ChannelMetadata on the websocket
        int64_t send_ChannelMetadata(ChannelMetadataPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChannelMetadata(ChannelMetadata&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the ChannelRemove on the websocket
        int64_t send_ChannelRemove(ChannelRemovePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChannelRemove(ChannelRemove&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the ChannelStatusChange on the websocket
        int64_t send_ChannelStatusChange(ChannelStatusChangePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChannelStatusChange(ChannelStatusChange&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<ChannelDescribe> SigChannelDescribe;
        etp_signal_t<ChannelRangeRequest> SigChannelRangeRequest;
        etp_signal_t<ChannelStreamingStart> SigChannelStreamingStart;
        etp_signal_t<ChannelStreamingStop> SigChannelStreamingStop;
        etp_signal_t<Start> SigStart;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace ChannelStreaming {

    /// Basic handler for protocol: 1 (ChannelStreaming), role: consumer
    class Consumer : public BaseHandler
    {
    public:    
        Consumer()
            : BaseHandler(1, "consumer")
        {        
            m_messages_handled.insert(3);
            m_messages_handled.insert(6);
            m_messages_handled.insert(2);
            m_messages_handled.insert(8);
            m_messages_handled.insert(10);
        }        
        virtual ~Consumer() {}
        
        /// Handle websocket messages for protocol: 1 (ChannelStreaming), role: consumer
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 3 :            
                SigChannelData(header, decode<ChannelData>(decoder));
                break;
            case 6 :            
                SigChannelDataChange(header, decode<ChannelDataChange>(decoder));
                break;
            case 2 :            
                SigChannelMetadata(header, decode<ChannelMetadata>(decoder));
                break;
            case 8 :            
                SigChannelRemove(header, decode<ChannelRemove>(decoder));
                break;
            case 10 :            
                SigChannelStatusChange(header, decode<ChannelStatusChange>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the ChannelData signal
        etp_sig_conn_t add_ChannelData_handler(const etp_slot_t<ChannelData>  &subscriber) { return SigChannelData.connect(subscriber); }
        /// connect to the ChannelDataChange signal
        etp_sig_conn_t add_ChannelDataChange_handler(const etp_slot_t<ChannelDataChange>  &subscriber) { return SigChannelDataChange.connect(subscriber); }
        /// connect to the ChannelMetadata signal
        etp_sig_conn_t add_ChannelMetadata_handler(const etp_slot_t<ChannelMetadata>  &subscriber) { return SigChannelMetadata.connect(subscriber); }
        /// connect to the ChannelRemove signal
        etp_sig_conn_t add_ChannelRemove_handler(const etp_slot_t<ChannelRemove>  &subscriber) { return SigChannelRemove.connect(subscriber); }
        /// connect to the ChannelStatusChange signal
        etp_sig_conn_t add_ChannelStatusChange_handler(const etp_slot_t<ChannelStatusChange>  &subscriber) { return SigChannelStatusChange.connect(subscriber); }
        
        // Message senders
        /// Send the ChannelDescribe on the websocket
        int64_t send_ChannelDescribe(ChannelDescribePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChannelDescribe(ChannelDescribe&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the ChannelRangeRequest on the websocket
        int64_t send_ChannelRangeRequest(ChannelRangeRequestPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChannelRangeRequest(ChannelRangeRequest&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the ChannelStreamingStart on the websocket
        int64_t send_ChannelStreamingStart(ChannelStreamingStartPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChannelStreamingStart(ChannelStreamingStart&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the ChannelStreamingStop on the websocket
        int64_t send_ChannelStreamingStop(ChannelStreamingStopPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChannelStreamingStop(ChannelStreamingStop&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the Start on the websocket
        int64_t send_Start(StartPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_Start(Start&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<ChannelData> SigChannelData;
        etp_signal_t<ChannelDataChange> SigChannelDataChange;
        etp_signal_t<ChannelMetadata> SigChannelMetadata;
        etp_signal_t<ChannelRemove> SigChannelRemove;
        etp_signal_t<ChannelStatusChange> SigChannelStatusChange;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace Core {

    /// Basic handler for protocol: 0 (Core), role: client
    class Client : public BaseHandler
    {
    public:    
        Client()
            : BaseHandler(0, "client")
        {        
            m_messages_handled.insert(1001);
            m_messages_handled.insert(5);
            m_messages_handled.insert(2);
            m_messages_handled.insert(1000);
        }        
        virtual ~Client() {}
        
        /// Handle websocket messages for protocol: 0 (Core), role: client
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 1001 :            
                SigAcknowledge(header, decode<Acknowledge>(decoder));
                break;
            case 5 :            
                SigCloseSession(header, decode<CloseSession>(decoder));
                break;
            case 2 :            
                SigOpenSession(header, decode<OpenSession>(decoder));
                break;
            case 1000 :            
                SigProtocolException(header, decode<ProtocolException>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the Acknowledge signal
        etp_sig_conn_t add_Acknowledge_handler(const etp_slot_t<Acknowledge>  &subscriber) { return SigAcknowledge.connect(subscriber); }
        /// connect to the CloseSession signal
        etp_sig_conn_t add_CloseSession_handler(const etp_slot_t<CloseSession>  &subscriber) { return SigCloseSession.connect(subscriber); }
        /// connect to the OpenSession signal
        etp_sig_conn_t add_OpenSession_handler(const etp_slot_t<OpenSession>  &subscriber) { return SigOpenSession.connect(subscriber); }
        /// connect to the ProtocolException signal
        etp_sig_conn_t add_ProtocolException_handler(const etp_slot_t<ProtocolException>  &subscriber) { return SigProtocolException.connect(subscriber); }
        
        // Message senders
        /// Send the Acknowledge on the websocket
        int64_t send_Acknowledge(AcknowledgePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_Acknowledge(Acknowledge&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the CloseSession on the websocket
        int64_t send_CloseSession(CloseSessionPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_CloseSession(CloseSession&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the ProtocolException on the websocket
        int64_t send_ProtocolException(ProtocolExceptionPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ProtocolException(ProtocolException&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the RequestSession on the websocket
        int64_t send_RequestSession(RequestSessionPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_RequestSession(RequestSession&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<Acknowledge> SigAcknowledge;
        etp_signal_t<CloseSession> SigCloseSession;
        etp_signal_t<OpenSession> SigOpenSession;
        etp_signal_t<ProtocolException> SigProtocolException;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace Core {

    /// Basic handler for protocol: 0 (Core), role: server
    class Server : public BaseHandler
    {
    public:    
        Server()
            : BaseHandler(0, "server")
        {        
            m_messages_handled.insert(1001);
            m_messages_handled.insert(5);
            m_messages_handled.insert(1000);
            m_messages_handled.insert(1);
        }        
        virtual ~Server() {}
        
        /// Handle websocket messages for protocol: 0 (Core), role: server
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 1001 :            
                SigAcknowledge(header, decode<Acknowledge>(decoder));
                break;
            case 5 :            
                SigCloseSession(header, decode<CloseSession>(decoder));
                break;
            case 1000 :            
                SigProtocolException(header, decode<ProtocolException>(decoder));
                break;
            case 1 :            
                SigRequestSession(header, decode<RequestSession>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the Acknowledge signal
        etp_sig_conn_t add_Acknowledge_handler(const etp_slot_t<Acknowledge>  &subscriber) { return SigAcknowledge.connect(subscriber); }
        /// connect to the CloseSession signal
        etp_sig_conn_t add_CloseSession_handler(const etp_slot_t<CloseSession>  &subscriber) { return SigCloseSession.connect(subscriber); }
        /// connect to the ProtocolException signal
        etp_sig_conn_t add_ProtocolException_handler(const etp_slot_t<ProtocolException>  &subscriber) { return SigProtocolException.connect(subscriber); }
        /// connect to the RequestSession signal
        etp_sig_conn_t add_RequestSession_handler(const etp_slot_t<RequestSession>  &subscriber) { return SigRequestSession.connect(subscriber); }
        
        // Message senders
        /// Send the Acknowledge on the websocket
        int64_t send_Acknowledge(AcknowledgePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_Acknowledge(Acknowledge&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the CloseSession on the websocket
        int64_t send_CloseSession(CloseSessionPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_CloseSession(CloseSession&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the OpenSession on the websocket
        int64_t send_OpenSession(OpenSessionPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_OpenSession(OpenSession&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the ProtocolException on the websocket
        int64_t send_ProtocolException(ProtocolExceptionPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ProtocolException(ProtocolException&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<Acknowledge> SigAcknowledge;
        etp_signal_t<CloseSession> SigCloseSession;
        etp_signal_t<ProtocolException> SigProtocolException;
        etp_signal_t<RequestSession> SigRequestSession;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace Discovery {

    /// Basic handler for protocol: 3 (Discovery), role: store
    class Store : public BaseHandler
    {
    public:    
        Store()
            : BaseHandler(3, "store")
        {        
            m_messages_handled.insert(1);
        }        
        virtual ~Store() {}
        
        /// Handle websocket messages for protocol: 3 (Discovery), role: store
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 1 :            
                SigGetResources(header, decode<GetResources>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the GetResources signal
        etp_sig_conn_t add_GetResources_handler(const etp_slot_t<GetResources>  &subscriber) { return SigGetResources.connect(subscriber); }
        
        // Message senders
        /// Send the GetResourcesResponse on the websocket
        int64_t send_GetResourcesResponse(GetResourcesResponsePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_GetResourcesResponse(GetResourcesResponse&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<GetResources> SigGetResources;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace Discovery {

    /// Basic handler for protocol: 3 (Discovery), role: customer
    class Customer : public BaseHandler
    {
    public:    
        Customer()
            : BaseHandler(3, "customer")
        {        
            m_messages_handled.insert(2);
        }        
        virtual ~Customer() {}
        
        /// Handle websocket messages for protocol: 3 (Discovery), role: customer
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 2 :            
                SigGetResourcesResponse(header, decode<GetResourcesResponse>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the GetResourcesResponse signal
        etp_sig_conn_t add_GetResourcesResponse_handler(const etp_slot_t<GetResourcesResponse>  &subscriber) { return SigGetResourcesResponse.connect(subscriber); }
        
        // Message senders
        /// Send the GetResources on the websocket
        int64_t send_GetResources(GetResourcesPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_GetResources(GetResources&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<GetResourcesResponse> SigGetResourcesResponse;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace DataArray {

    /// Basic handler for protocol: 7 (DataArray), role: store
    class Store : public BaseHandler
    {
    public:    
        Store()
            : BaseHandler(7, "store")
        {        
            m_messages_handled.insert(2);
            m_messages_handled.insert(3);
            m_messages_handled.insert(4);
            m_messages_handled.insert(5);
        }        
        virtual ~Store() {}
        
        /// Handle websocket messages for protocol: 7 (DataArray), role: store
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 2 :            
                SigGetDataArray(header, decode<GetDataArray>(decoder));
                break;
            case 3 :            
                SigGetDataArraySlice(header, decode<GetDataArraySlice>(decoder));
                break;
            case 4 :            
                SigPutDataArray(header, decode<PutDataArray>(decoder));
                break;
            case 5 :            
                SigPutDataArraySlice(header, decode<PutDataArraySlice>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the GetDataArray signal
        etp_sig_conn_t add_GetDataArray_handler(const etp_slot_t<GetDataArray>  &subscriber) { return SigGetDataArray.connect(subscriber); }
        /// connect to the GetDataArraySlice signal
        etp_sig_conn_t add_GetDataArraySlice_handler(const etp_slot_t<GetDataArraySlice>  &subscriber) { return SigGetDataArraySlice.connect(subscriber); }
        /// connect to the PutDataArray signal
        etp_sig_conn_t add_PutDataArray_handler(const etp_slot_t<PutDataArray>  &subscriber) { return SigPutDataArray.connect(subscriber); }
        /// connect to the PutDataArraySlice signal
        etp_sig_conn_t add_PutDataArraySlice_handler(const etp_slot_t<PutDataArraySlice>  &subscriber) { return SigPutDataArraySlice.connect(subscriber); }
        
        // Message senders
        /// Send the DataArray on the websocket
        int64_t send_DataArray(DataArrayPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_DataArray(DataArray&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<GetDataArray> SigGetDataArray;
        etp_signal_t<GetDataArraySlice> SigGetDataArraySlice;
        etp_signal_t<PutDataArray> SigPutDataArray;
        etp_signal_t<PutDataArraySlice> SigPutDataArraySlice;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace DataArray {

    /// Basic handler for protocol: 7 (DataArray), role: customer
    class Customer : public BaseHandler
    {
    public:    
        Customer()
            : BaseHandler(7, "customer")
        {        
            m_messages_handled.insert(1);
        }        
        virtual ~Customer() {}
        
        /// Handle websocket messages for protocol: 7 (DataArray), role: customer
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 1 :            
                SigDataArray(header, decode<DataArray>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the DataArray signal
        etp_sig_conn_t add_DataArray_handler(const etp_slot_t<DataArray>  &subscriber) { return SigDataArray.connect(subscriber); }
        
        // Message senders
        /// Send the GetDataArray on the websocket
        int64_t send_GetDataArray(GetDataArrayPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_GetDataArray(GetDataArray&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the GetDataArraySlice on the websocket
        int64_t send_GetDataArraySlice(GetDataArraySlicePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_GetDataArraySlice(GetDataArraySlice&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the PutDataArray on the websocket
        int64_t send_PutDataArray(PutDataArrayPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_PutDataArray(PutDataArray&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the PutDataArraySlice on the websocket
        int64_t send_PutDataArraySlice(PutDataArraySlicePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_PutDataArraySlice(PutDataArraySlice&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<DataArray> SigDataArray;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace GrowingObject {

    /// Basic handler for protocol: 6 (GrowingObject), role: store
    class Store : public BaseHandler
    {
    public:    
        Store()
            : BaseHandler(6, "store")
        {        
            m_messages_handled.insert(2);
            m_messages_handled.insert(4);
            m_messages_handled.insert(3);
            m_messages_handled.insert(5);
            m_messages_handled.insert(1);
        }        
        virtual ~Store() {}
        
        /// Handle websocket messages for protocol: 6 (GrowingObject), role: store
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 2 :            
                SigGrowingObjectDeleteRange(header, decode<GrowingObjectDeleteRange>(decoder));
                break;
            case 4 :            
                SigGrowingObjectGetRange(header, decode<GrowingObjectGetRange>(decoder));
                break;
            case 3 :            
                SigGrowingObjectGet(header, decode<GrowingObjectGet>(decoder));
                break;
            case 5 :            
                SigGrowingObjectPut(header, decode<GrowingObjectPut>(decoder));
                break;
            case 1 :            
                SigGrowingObjectDelete(header, decode<GrowingObjectDelete>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the GrowingObjectDeleteRange signal
        etp_sig_conn_t add_GrowingObjectDeleteRange_handler(const etp_slot_t<GrowingObjectDeleteRange>  &subscriber) { return SigGrowingObjectDeleteRange.connect(subscriber); }
        /// connect to the GrowingObjectGetRange signal
        etp_sig_conn_t add_GrowingObjectGetRange_handler(const etp_slot_t<GrowingObjectGetRange>  &subscriber) { return SigGrowingObjectGetRange.connect(subscriber); }
        /// connect to the GrowingObjectGet signal
        etp_sig_conn_t add_GrowingObjectGet_handler(const etp_slot_t<GrowingObjectGet>  &subscriber) { return SigGrowingObjectGet.connect(subscriber); }
        /// connect to the GrowingObjectPut signal
        etp_sig_conn_t add_GrowingObjectPut_handler(const etp_slot_t<GrowingObjectPut>  &subscriber) { return SigGrowingObjectPut.connect(subscriber); }
        /// connect to the GrowingObjectDelete signal
        etp_sig_conn_t add_GrowingObjectDelete_handler(const etp_slot_t<GrowingObjectDelete>  &subscriber) { return SigGrowingObjectDelete.connect(subscriber); }
        
        // Message senders
        /// Send the ObjectFragment on the websocket
        int64_t send_ObjectFragment(ObjectFragmentPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ObjectFragment(ObjectFragment&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<GrowingObjectDeleteRange> SigGrowingObjectDeleteRange;
        etp_signal_t<GrowingObjectGetRange> SigGrowingObjectGetRange;
        etp_signal_t<GrowingObjectGet> SigGrowingObjectGet;
        etp_signal_t<GrowingObjectPut> SigGrowingObjectPut;
        etp_signal_t<GrowingObjectDelete> SigGrowingObjectDelete;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace GrowingObject {

    /// Basic handler for protocol: 6 (GrowingObject), role: customer
    class Customer : public BaseHandler
    {
    public:    
        Customer()
            : BaseHandler(6, "customer")
        {        
            m_messages_handled.insert(6);
        }        
        virtual ~Customer() {}
        
        /// Handle websocket messages for protocol: 6 (GrowingObject), role: customer
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 6 :            
                SigObjectFragment(header, decode<ObjectFragment>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the ObjectFragment signal
        etp_sig_conn_t add_ObjectFragment_handler(const etp_slot_t<ObjectFragment>  &subscriber) { return SigObjectFragment.connect(subscriber); }
        
        // Message senders
        /// Send the GrowingObjectDeleteRange on the websocket
        int64_t send_GrowingObjectDeleteRange(GrowingObjectDeleteRangePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_GrowingObjectDeleteRange(GrowingObjectDeleteRange&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the GrowingObjectGetRange on the websocket
        int64_t send_GrowingObjectGetRange(GrowingObjectGetRangePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_GrowingObjectGetRange(GrowingObjectGetRange&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the GrowingObjectGet on the websocket
        int64_t send_GrowingObjectGet(GrowingObjectGetPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_GrowingObjectGet(GrowingObjectGet&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the GrowingObjectPut on the websocket
        int64_t send_GrowingObjectPut(GrowingObjectPutPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_GrowingObjectPut(GrowingObjectPut&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the GrowingObjectDelete on the websocket
        int64_t send_GrowingObjectDelete(GrowingObjectDeletePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_GrowingObjectDelete(GrowingObjectDelete&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<ObjectFragment> SigObjectFragment;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace Store {

    /// Basic handler for protocol: 4 (Store), role: store
    class Store : public BaseHandler
    {
    public:    
        Store()
            : BaseHandler(4, "store")
        {        
            m_messages_handled.insert(3);
            m_messages_handled.insert(1);
            m_messages_handled.insert(2);
        }        
        virtual ~Store() {}
        
        /// Handle websocket messages for protocol: 4 (Store), role: store
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 3 :            
                SigDeleteObject(header, decode<DeleteObject>(decoder));
                break;
            case 1 :            
                SigGetObject(header, decode<GetObject>(decoder));
                break;
            case 2 :            
                SigPutObject(header, decode<PutObject>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the DeleteObject signal
        etp_sig_conn_t add_DeleteObject_handler(const etp_slot_t<DeleteObject>  &subscriber) { return SigDeleteObject.connect(subscriber); }
        /// connect to the GetObject signal
        etp_sig_conn_t add_GetObject_handler(const etp_slot_t<GetObject>  &subscriber) { return SigGetObject.connect(subscriber); }
        /// connect to the PutObject signal
        etp_sig_conn_t add_PutObject_handler(const etp_slot_t<PutObject>  &subscriber) { return SigPutObject.connect(subscriber); }
        
        // Message senders
        /// Send the Object on the websocket
        int64_t send_Object(ObjectPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_Object(Object&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<DeleteObject> SigDeleteObject;
        etp_signal_t<GetObject> SigGetObject;
        etp_signal_t<PutObject> SigPutObject;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace Store {

    /// Basic handler for protocol: 4 (Store), role: customer
    class Customer : public BaseHandler
    {
    public:    
        Customer()
            : BaseHandler(4, "customer")
        {        
            m_messages_handled.insert(4);
        }        
        virtual ~Customer() {}
        
        /// Handle websocket messages for protocol: 4 (Store), role: customer
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 4 :            
                SigObject(header, decode<Object>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the Object signal
        etp_sig_conn_t add_Object_handler(const etp_slot_t<Object>  &subscriber) { return SigObject.connect(subscriber); }
        
        // Message senders
        /// Send the DeleteObject on the websocket
        int64_t send_DeleteObject(DeleteObjectPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_DeleteObject(DeleteObject&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the GetObject on the websocket
        int64_t send_GetObject(GetObjectPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_GetObject(GetObject&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the PutObject on the websocket
        int64_t send_PutObject(PutObjectPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_PutObject(PutObject&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<Object> SigObject;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace StoreNotification {

    /// Basic handler for protocol: 5 (StoreNotification), role: store
    class Store : public BaseHandler
    {
    public:    
        Store()
            : BaseHandler(5, "store")
        {        
            m_messages_handled.insert(4);
            m_messages_handled.insert(1);
        }        
        virtual ~Store() {}
        
        /// Handle websocket messages for protocol: 5 (StoreNotification), role: store
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 4 :            
                SigCancelNotification(header, decode<CancelNotification>(decoder));
                break;
            case 1 :            
                SigNotificationRequest(header, decode<NotificationRequest>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the CancelNotification signal
        etp_sig_conn_t add_CancelNotification_handler(const etp_slot_t<CancelNotification>  &subscriber) { return SigCancelNotification.connect(subscriber); }
        /// connect to the NotificationRequest signal
        etp_sig_conn_t add_NotificationRequest_handler(const etp_slot_t<NotificationRequest>  &subscriber) { return SigNotificationRequest.connect(subscriber); }
        
        // Message senders
        /// Send the DeleteNotification on the websocket
        int64_t send_DeleteNotification(DeleteNotificationPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_DeleteNotification(DeleteNotification&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the ChangeNotification on the websocket
        int64_t send_ChangeNotification(ChangeNotificationPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_ChangeNotification(ChangeNotification&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<CancelNotification> SigCancelNotification;
        etp_signal_t<NotificationRequest> SigNotificationRequest;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace StoreNotification {

    /// Basic handler for protocol: 5 (StoreNotification), role: customer
    class Customer : public BaseHandler
    {
    public:    
        Customer()
            : BaseHandler(5, "customer")
        {        
            m_messages_handled.insert(3);
            m_messages_handled.insert(2);
        }        
        virtual ~Customer() {}
        
        /// Handle websocket messages for protocol: 5 (StoreNotification), role: customer
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 3 :            
                SigDeleteNotification(header, decode<DeleteNotification>(decoder));
                break;
            case 2 :            
                SigChangeNotification(header, decode<ChangeNotification>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the DeleteNotification signal
        etp_sig_conn_t add_DeleteNotification_handler(const etp_slot_t<DeleteNotification>  &subscriber) { return SigDeleteNotification.connect(subscriber); }
        /// connect to the ChangeNotification signal
        etp_sig_conn_t add_ChangeNotification_handler(const etp_slot_t<ChangeNotification>  &subscriber) { return SigChangeNotification.connect(subscriber); }
        
        // Message senders
        /// Send the CancelNotification on the websocket
        int64_t send_CancelNotification(CancelNotificationPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_CancelNotification(CancelNotification&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the NotificationRequest on the websocket
        int64_t send_NotificationRequest(NotificationRequestPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_NotificationRequest(NotificationRequest&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<DeleteNotification> SigDeleteNotification;
        etp_signal_t<ChangeNotification> SigChangeNotification;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace WitsmlSoap {

    /// Basic handler for protocol: 8 (WitsmlSoap), role: store
    class Store : public BaseHandler
    {
    public:    
        Store()
            : BaseHandler(8, "store")
        {        
            m_messages_handled.insert(1);
            m_messages_handled.insert(3);
            m_messages_handled.insert(5);
            m_messages_handled.insert(7);
            m_messages_handled.insert(9);
            m_messages_handled.insert(11);
            m_messages_handled.insert(13);
        }        
        virtual ~Store() {}
        
        /// Handle websocket messages for protocol: 8 (WitsmlSoap), role: store
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 1 :            
                SigWMLS_AddToStore(header, decode<WMLS_AddToStore>(decoder));
                break;
            case 3 :            
                SigWMLS_DeleteFromStore(header, decode<WMLS_DeleteFromStore>(decoder));
                break;
            case 5 :            
                SigWMLS_GetBaseMsg(header, decode<WMLS_GetBaseMsg>(decoder));
                break;
            case 7 :            
                SigWMLS_GetCap(header, decode<WMLS_GetCap>(decoder));
                break;
            case 9 :            
                SigWMLS_GetFromStore(header, decode<WMLS_GetFromStore>(decoder));
                break;
            case 11 :            
                SigWMLS_GetVersion(header, decode<WMLS_GetVersion>(decoder));
                break;
            case 13 :            
                SigWMLS_UpdateInStore(header, decode<WMLS_UpdateInStore>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the WMLS_AddToStore signal
        etp_sig_conn_t add_WMLS_AddToStore_handler(const etp_slot_t<WMLS_AddToStore>  &subscriber) { return SigWMLS_AddToStore.connect(subscriber); }
        /// connect to the WMLS_DeleteFromStore signal
        etp_sig_conn_t add_WMLS_DeleteFromStore_handler(const etp_slot_t<WMLS_DeleteFromStore>  &subscriber) { return SigWMLS_DeleteFromStore.connect(subscriber); }
        /// connect to the WMLS_GetBaseMsg signal
        etp_sig_conn_t add_WMLS_GetBaseMsg_handler(const etp_slot_t<WMLS_GetBaseMsg>  &subscriber) { return SigWMLS_GetBaseMsg.connect(subscriber); }
        /// connect to the WMLS_GetCap signal
        etp_sig_conn_t add_WMLS_GetCap_handler(const etp_slot_t<WMLS_GetCap>  &subscriber) { return SigWMLS_GetCap.connect(subscriber); }
        /// connect to the WMLS_GetFromStore signal
        etp_sig_conn_t add_WMLS_GetFromStore_handler(const etp_slot_t<WMLS_GetFromStore>  &subscriber) { return SigWMLS_GetFromStore.connect(subscriber); }
        /// connect to the WMLS_GetVersion signal
        etp_sig_conn_t add_WMLS_GetVersion_handler(const etp_slot_t<WMLS_GetVersion>  &subscriber) { return SigWMLS_GetVersion.connect(subscriber); }
        /// connect to the WMLS_UpdateInStore signal
        etp_sig_conn_t add_WMLS_UpdateInStore_handler(const etp_slot_t<WMLS_UpdateInStore>  &subscriber) { return SigWMLS_UpdateInStore.connect(subscriber); }
        
        // Message senders
        /// Send the WMSL_AddToStoreResponse on the websocket
        int64_t send_WMSL_AddToStoreResponse(WMSL_AddToStoreResponsePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMSL_AddToStoreResponse(WMSL_AddToStoreResponse&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMSL_DeleteFromStoreResponse on the websocket
        int64_t send_WMSL_DeleteFromStoreResponse(WMSL_DeleteFromStoreResponsePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMSL_DeleteFromStoreResponse(WMSL_DeleteFromStoreResponse&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMSL_GetBaseMsgResponse on the websocket
        int64_t send_WMSL_GetBaseMsgResponse(WMSL_GetBaseMsgResponsePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMSL_GetBaseMsgResponse(WMSL_GetBaseMsgResponse&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMSL_GetCapResponse on the websocket
        int64_t send_WMSL_GetCapResponse(WMSL_GetCapResponsePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMSL_GetCapResponse(WMSL_GetCapResponse&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMSL_GetFromStoreResponse on the websocket
        int64_t send_WMSL_GetFromStoreResponse(WMSL_GetFromStoreResponsePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMSL_GetFromStoreResponse(WMSL_GetFromStoreResponse&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMSL_GetVersionResponse on the websocket
        int64_t send_WMSL_GetVersionResponse(WMSL_GetVersionResponsePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMSL_GetVersionResponse(WMSL_GetVersionResponse&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMSL_UpdateInStoreResponse on the websocket
        int64_t send_WMSL_UpdateInStoreResponse(WMSL_UpdateInStoreResponsePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMSL_UpdateInStoreResponse(WMSL_UpdateInStoreResponse&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<WMLS_AddToStore> SigWMLS_AddToStore;
        etp_signal_t<WMLS_DeleteFromStore> SigWMLS_DeleteFromStore;
        etp_signal_t<WMLS_GetBaseMsg> SigWMLS_GetBaseMsg;
        etp_signal_t<WMLS_GetCap> SigWMLS_GetCap;
        etp_signal_t<WMLS_GetFromStore> SigWMLS_GetFromStore;
        etp_signal_t<WMLS_GetVersion> SigWMLS_GetVersion;
        etp_signal_t<WMLS_UpdateInStore> SigWMLS_UpdateInStore;
    };
}
}
}

namespace Energistics {
namespace Protocol {
namespace WitsmlSoap {

    /// Basic handler for protocol: 8 (WitsmlSoap), role: customer
    class Customer : public BaseHandler
    {
    public:    
        Customer()
            : BaseHandler(8, "customer")
        {        
            m_messages_handled.insert(2);
            m_messages_handled.insert(4);
            m_messages_handled.insert(6);
            m_messages_handled.insert(8);
            m_messages_handled.insert(10);
            m_messages_handled.insert(12);
            m_messages_handled.insert(14);
        }        
        virtual ~Customer() {}
        
        /// Handle websocket messages for protocol: 8 (WitsmlSoap), role: customer
        void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {        
            switch (header.m_messageType) {
            case 2 :            
                SigWMSL_AddToStoreResponse(header, decode<WMSL_AddToStoreResponse>(decoder));
                break;
            case 4 :            
                SigWMSL_DeleteFromStoreResponse(header, decode<WMSL_DeleteFromStoreResponse>(decoder));
                break;
            case 6 :            
                SigWMSL_GetBaseMsgResponse(header, decode<WMSL_GetBaseMsgResponse>(decoder));
                break;
            case 8 :            
                SigWMSL_GetCapResponse(header, decode<WMSL_GetCapResponse>(decoder));
                break;
            case 10 :            
                SigWMSL_GetFromStoreResponse(header, decode<WMSL_GetFromStoreResponse>(decoder));
                break;
            case 12 :            
                SigWMSL_GetVersionResponse(header, decode<WMSL_GetVersionResponse>(decoder));
                break;
            case 14 :            
                SigWMSL_UpdateInStoreResponse(header, decode<WMSL_UpdateInStoreResponse>(decoder));
                break;
            default:            
                BaseHandler::handle_message(header, decoder);
                break;
            }
        }
        
        
        // Event connectors
        /// connect to the WMSL_AddToStoreResponse signal
        etp_sig_conn_t add_WMSL_AddToStoreResponse_handler(const etp_slot_t<WMSL_AddToStoreResponse>  &subscriber) { return SigWMSL_AddToStoreResponse.connect(subscriber); }
        /// connect to the WMSL_DeleteFromStoreResponse signal
        etp_sig_conn_t add_WMSL_DeleteFromStoreResponse_handler(const etp_slot_t<WMSL_DeleteFromStoreResponse>  &subscriber) { return SigWMSL_DeleteFromStoreResponse.connect(subscriber); }
        /// connect to the WMSL_GetBaseMsgResponse signal
        etp_sig_conn_t add_WMSL_GetBaseMsgResponse_handler(const etp_slot_t<WMSL_GetBaseMsgResponse>  &subscriber) { return SigWMSL_GetBaseMsgResponse.connect(subscriber); }
        /// connect to the WMSL_GetCapResponse signal
        etp_sig_conn_t add_WMSL_GetCapResponse_handler(const etp_slot_t<WMSL_GetCapResponse>  &subscriber) { return SigWMSL_GetCapResponse.connect(subscriber); }
        /// connect to the WMSL_GetFromStoreResponse signal
        etp_sig_conn_t add_WMSL_GetFromStoreResponse_handler(const etp_slot_t<WMSL_GetFromStoreResponse>  &subscriber) { return SigWMSL_GetFromStoreResponse.connect(subscriber); }
        /// connect to the WMSL_GetVersionResponse signal
        etp_sig_conn_t add_WMSL_GetVersionResponse_handler(const etp_slot_t<WMSL_GetVersionResponse>  &subscriber) { return SigWMSL_GetVersionResponse.connect(subscriber); }
        /// connect to the WMSL_UpdateInStoreResponse signal
        etp_sig_conn_t add_WMSL_UpdateInStoreResponse_handler(const etp_slot_t<WMSL_UpdateInStoreResponse>  &subscriber) { return SigWMSL_UpdateInStoreResponse.connect(subscriber); }
        
        // Message senders
        /// Send the WMLS_AddToStore on the websocket
        int64_t send_WMLS_AddToStore(WMLS_AddToStorePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMLS_AddToStore(WMLS_AddToStore&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMLS_DeleteFromStore on the websocket
        int64_t send_WMLS_DeleteFromStore(WMLS_DeleteFromStorePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMLS_DeleteFromStore(WMLS_DeleteFromStore&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMLS_GetBaseMsg on the websocket
        int64_t send_WMLS_GetBaseMsg(WMLS_GetBaseMsgPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMLS_GetBaseMsg(WMLS_GetBaseMsg&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMLS_GetCap on the websocket
        int64_t send_WMLS_GetCap(WMLS_GetCapPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMLS_GetCap(WMLS_GetCap&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMLS_GetFromStore on the websocket
        int64_t send_WMLS_GetFromStore(WMLS_GetFromStorePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMLS_GetFromStore(WMLS_GetFromStore&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMLS_GetVersion on the websocket
        int64_t send_WMLS_GetVersion(WMLS_GetVersionPtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMLS_GetVersion(WMLS_GetVersion&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        /// Send the WMLS_UpdateInStore on the websocket
        int64_t send_WMLS_UpdateInStore(WMLS_UpdateInStorePtr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
        int64_t send_WMLS_UpdateInStore(WMLS_UpdateInStore&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }
    public:    
        etp_signal_t<WMSL_AddToStoreResponse> SigWMSL_AddToStoreResponse;
        etp_signal_t<WMSL_DeleteFromStoreResponse> SigWMSL_DeleteFromStoreResponse;
        etp_signal_t<WMSL_GetBaseMsgResponse> SigWMSL_GetBaseMsgResponse;
        etp_signal_t<WMSL_GetCapResponse> SigWMSL_GetCapResponse;
        etp_signal_t<WMSL_GetFromStoreResponse> SigWMSL_GetFromStoreResponse;
        etp_signal_t<WMSL_GetVersionResponse> SigWMSL_GetVersionResponse;
        etp_signal_t<WMSL_UpdateInStoreResponse> SigWMSL_UpdateInStoreResponse;
    };
}
}
}

#endif    // ETP_MESSAGE_HANDLERS_HPP_
