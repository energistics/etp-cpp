#include "EtpMessageHandlers.hpp"

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

using namespace Energistics::Protocol::Core;
using namespace Energistics::Datatypes;

TEST_CASE( "Serialize", "[Serialize]" )
{
	//std::map<std::string, std::string> protocolCapabilities;
	//protocolCapabilities.insert("foo", "bar");
	Version myVersion = {1, 0, 0, 1};
	SupportedProtocol sp1;
	sp1.m_protocol = 0;
	sp1.m_protocolVersion = myVersion;
	sp1.m_role = "client";

	std::vector<SupportedProtocol> rp;
	rp.push_back(sp1);

	RequestSession rs;
	rs.m_applicationName = "EtpCppCommonTests";
	rs.m_requestedProtocols = rp;


    std::unique_ptr<avro::OutputStream> out = avro::memoryOutputStream();
    avro::EncoderPtr e = avro::binaryEncoder();
    e->init(*out);
	avro::encode(*e, rs);
	

	RequestSession rs1;
    std::unique_ptr<avro::InputStream> in = avro::memoryInputStream(*out);
    avro::DecoderPtr d = avro::binaryDecoder();
    d->init(*in);
	avro::decode(*d, rs1);
	
	REQUIRE(rs1.m_requestedProtocols.size()==1);
	REQUIRE(rs1.m_applicationName == "EtpCppCommonTests");
	REQUIRE(rs1.m_requestedProtocols[0].m_role=="client");

}

TEST_CASE("Resource")
{
	Energistics::Datatypes::Object::Resource r;
    std::unique_ptr<avro::OutputStream> out = avro::memoryOutputStream();
    avro::EncoderPtr e = avro::binaryEncoder();
    e->init(*out);
	avro::encode(*e, r);

	Energistics::Datatypes::Object::Resource r1;
    std::unique_ptr<avro::InputStream> in = avro::memoryInputStream(*out);
    avro::DecoderPtr d = avro::binaryDecoder();
    d->init(*in);
	avro::decode(*d, r1);
	
	
}

TEST_CASE( "Union" )
{
	namespace dt = Energistics::Datatypes;
	
	dt::DataValue dv;
	
	REQUIRE(dv.m_item.idx()==0);
	
	dv.m_item.set_long(7L);
	REQUIRE(dv.m_item.get_long() == 7L);
	REQUIRE(dv.m_item.idx()==4);
	
	int64_t& ref = dv.m_item.get_long();
	REQUIRE(ref == 7L);
	
	ref++;
	
	REQUIRE(dv.m_item.get_long() == 8L);
	
	
	
}

TEST_CASE( "Array" )
{
	namespace dt = Energistics::Datatypes;
	
	dt::DataValue dv;
	dv.m_item.set_ArrayOfDouble( ArrayOfDouble() );
	REQUIRE( dv.m_item.idx() == 6);
	
	ArrayOfDouble& arr = dv.m_item.get_ArrayOfDouble();
	REQUIRE(arr.m_values.size()==0);
	for (double i=0; i<1000; i++) {
		arr.m_values.push_back(i);	
	}
	REQUIRE(arr.m_values.size() == 1000);
	
	// Reserve doesnt make the size bigger.
	arr.m_values.resize(100000);
	REQUIRE(arr.m_values.size() == 100000);
	
	// Use the space as a raw array
	double* dptr = &arr.m_values[0];
	for (size_t i=0; i<100000; i++) {
		dptr[i] = (double)i;	
	}
	
	REQUIRE(arr.m_values[50000] == 50000.);
	
	
}
