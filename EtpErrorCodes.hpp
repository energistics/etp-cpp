#ifndef ETP_ERRORCODES_HPP_
#define ETP_ERRORCODES_HPP_

namespace Energistics {

	namespace Datatypes {
		enum ErrorCodes {		
			ENOROLE=0,
			ENOSUPPORTEDPROTOCOLS=1,
			EINVALID_MESSAGETYPE=2,
			EUNSUPPORTED_PROTOCOL=3
		};		
	}
	
};

#endif