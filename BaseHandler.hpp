#ifndef BASEHANDLER_HPP_
#define BASEHANDLER_HPP_

#include <iostream>
#include <signal.h>
#include <functional>
#include <memory>
#include <map>
#include <set>
#include <cctype>
#include <iomanip>
#include <sstream>
#include <string>

#include <boost/make_shared.hpp>
#include <boost/signals2.hpp>
#include <boost/any.hpp>

// Work around for nasty MS Macro
#ifdef GetObject
# undef GetObject
#endif

// Work around for nasty osx behavior with std::vector of bool
#ifdef __APPLE__
#include <avro/Specific.hh>
#include <avro/Encoder.hh>
#include <avro/Decoder.hh>
namespace avro {
    static void encode(avro::Encoder& e, const std::vector<bool>& b) {
        e.arrayStart();
        if (! b.empty()) {
            e.setItemCount(b.size());
            for (bool val : b ) {
                e.startItem();
                avro::encode(e, val);
            }
        }
        e.arrayEnd();
    }
}
#endif


#include "EtpMessages.hpp"

struct null_deleter
{
	void operator()(void const *) const
	{
	}
};


struct ProtocolRole {
	ProtocolRole(int32_t protocol, std::string role) :
		m_protocol(protocol),
		m_role(role)
	{
	}
	const int32_t m_protocol;
	const std::string m_role;
	friend bool operator< (const ProtocolRole &lhs, const ProtocolRole &rhs) {
		return std::tie(lhs.m_protocol, lhs.m_role) < std::tie(rhs.m_protocol, rhs.m_role);	
	}
};

typedef boost::shared_ptr<std::string> etp_buffer_ptr;

#include "EtpMessages.hpp"



namespace Etp {
	using Energistics::Datatypes::MessageHeader;

	template <class T>
	using etp_shared_ptr = typename boost::shared_ptr<T>;

	typedef boost::shared_ptr<std::string> etp_buffer_ptr;

	template<class T>
	using etp_msg_ptr = typename boost::shared_ptr<T>;

	template<class T>
	using etp_signal_t = typename boost::signals2::signal<void(Energistics::Datatypes::MessageHeader header, etp_msg_ptr<T> message)>;

	template<class T>
	using etp_slot_t = typename boost::signals2::signal<void(Energistics::Datatypes::MessageHeader header, etp_msg_ptr<T> message)>::slot_type;

	typedef boost::signals2::connection etp_sig_conn_t;

	typedef boost::signals2::signal<void (etp_buffer_ptr)> sender_signal_t;

	struct ProtocolRole {
		ProtocolRole(int32_t protocol, std::string role) :
			m_protocol(protocol),
			m_role(role)
		{
		}
		const int32_t m_protocol;
		const std::string m_role;
		friend bool operator< (const ProtocolRole &lhs, const ProtocolRole &rhs) {
			return std::tie(lhs.m_protocol, lhs.m_role) < std::tie(rhs.m_protocol, rhs.m_role);	
		}
	};

	namespace Encodings {
		enum values {
			EtpBinary = 0,
			EtpJson = 1
		};
	}
	
	/// Abstract base class for all protocol handler.
	class BaseHandler
	{
	public:
		BaseHandler(int protocol, std::string role) :
			 m_protocol(protocol),
			 m_role(role)
		{
            m_encoding = Etp::Encodings::values::EtpBinary;
		}
		virtual ~BaseHandler(){};

		/// Allows us to subscribe to send signals
        etp_sig_conn_t connect(const sender_signal_t::slot_type  &subscriber) { return on_send.connect(subscriber); }

		/// Generate a new message header object.
		MessageHeader create_header(int32_t protocol_id, int32_t message_type_id, int64_t correlation_id=0, int messageFlags=0)
		{
			MessageHeader header;
			header.m_correlationId = correlation_id;
			header.m_messageFlags = messageFlags;
			header.m_messageId = new_message_id();
			header.m_protocol = protocol_id;
			header.m_messageType = message_type_id;
			return header;
		}
		
		bool handles(MessageHeader header) {
			return ( header.m_protocol==get_protocol_id() 
				&&  m_messages_handled.find(header.m_messageType)!=m_messages_handled.end() );
		}
		
		/// Handle a half-parsed message. 
		/** The Session object has parsed the header,
		 * uses the protocol_id to find an appropriate handler and passes the decoder
		 * on to it. Override in all protocol handlers to parse your know message types.
		 */
		virtual void handle_message(MessageHeader header, avro::DecoderPtr decoder)
		{
			// The abstract base should never handle a message.
			send_exception(Energistics::Datatypes::ErrorCodes::EINVALID_MESSAGETYPE, header.m_messageId, "Invalid MessageTypeId");
		};
		
		/// Encoding for this session. Must be the same for all messages.
        bool is_binary()
        {
            return m_encoding == Etp::Encodings::values::EtpBinary;
        }
		

		/// Decode any typed message from etp-cpp
		template<class T> etp_msg_ptr<T> decode(avro::DecoderPtr decoder) {
			T* message=new T();
			avro::decode(*decoder, *message);
			return etp_msg_ptr<T>(message);
		}

        /// The ETP protocol number that we handle
		int get_protocol_id()
		{
			return m_protocol;
		}

		/// Return the role string associated with this protocol handler.
		std::string get_role()
		{
			return m_role;
		}
		
		/// Get the protocol and role as a combined structure.
		/**
		 * This uniquely identifies the protocol handler for handling one end of a protocol.
		 */
		ProtocolRole get_protocol_role()
		{
			return ProtocolRole(m_protocol, m_role);
		}

		/// Default message id provider, using thread-safe static counter. Normally will be over-ridden.
        virtual int64_t new_message_id()
		{
			static long _messageId;
			int64_t retval =_messageId++;
			return retval;
		}
				
		/// Send a specific message
        template<class T> int64_t send(MessageHeader& messageHeader, T& messageBody)
        {			
			etp_shared_ptr<T> ptr = etp_shared_ptr<T>(&messageBody, null_deleter());
			return send(messageHeader, ptr);
        }
		
		
		/// Send a specific message
        template<class T> int64_t send(MessageHeader& messageHeader, boost::shared_ptr<T> messageBody)
        {			
            avro::EncoderPtr enc;
            if(is_binary()) {
                enc = avro::binaryEncoder();
            }
            else {
				/// TODO handle json
                //enc = avro::jsonEncoder();
            }
            std::auto_ptr<avro::OutputStream> out = avro::memoryOutputStream();
            enc->init(*out);
            avro::encode(*enc, messageHeader);
            avro::encode(*enc, *messageBody);
            out->flush();
            size_t len = out->byteCount();
			std::auto_ptr<avro::InputStream> in = avro::memoryInputStream(*out);
			avro::StreamReader* reader = new avro::StreamReader(*in);

			std::string* bytes = new std::string();
			bytes->reserve(len);

			while (reader->hasMore()) {
					uint8_t c = reader->read();
					bytes->push_back(c);
			}
			
            on_send(etp_buffer_ptr(bytes));
			
			return messageHeader.m_messageId;
        }

		template<class T> int64_t send(T& message, int64_t correlation_id=0, int32_t flags=0)
		{
			etp_shared_ptr<T> ptr = etp_shared_ptr<T>(&message, null_deleter());
			return send(ptr, correlation_id, flags);
		}
		
		template<class T> int64_t send(etp_shared_ptr<T> message, int64_t correlation_id=0, int32_t flags=0)
		{
			MessageHeader mh = create_header(get_protocol_id(), message->messageTypeId, correlation_id, flags);
			send(mh, message);
			return mh.m_messageId;
		}
		/// Send a protocol exception
		int64_t send_exception(int32_t error_code, int64_t correlation_id, std::string message)
		{
			/// Exception has its own messageTypeId that is used for all protocols.
			MessageHeader mh = create_header(m_protocol, Energistics::Protocol::Core::PROTOCOLEXCEPTION, correlation_id);
			auto exp = new Energistics::Protocol::Core::ProtocolException();
			exp->m_errorCode = error_code;
			exp->m_errorMessage = message;
			send(mh, etp_shared_ptr<Energistics::Protocol::Core::ProtocolException>(exp));
			return mh.m_messageId;
		}

		/// Save the value of the supported protocol record, to get access to capablities, etc.
		void set_supported_protocol(Energistics::Datatypes::SupportedProtocol sp) {
			_supported_protocol = sp;			
		}
		
		/// Start the protocol. Individual implementation handlers override this for startup behavior.
		/// Protocol 1 should send the start message at this point.
		virtual void start() { }
		
	    /// decode a uri
		static std::string uri_decode(const std::string & value) {
			std::string ret;
			char ch;
			int i, ii;
			for (i=0; i<value.length(); i++) {
				if (int(value[i])=='%') {
					sscanf(value.substr(i+1,2).c_str(), "%x", &ii);
					ch=static_cast<char>(ii);
					ret+=ch;
					i=i+2;
				} else {
					ret+=value[i];
				}
			}
			return (ret);
			
		}

		
		/// encode a uri
	    static std::string uri_encode(const std::string & value)
		{
			std::ostringstream escaped;
			escaped.fill('0');
			escaped << std::hex;

			for (std::string::const_iterator i = value.begin(), n = value.end(); i != n; ++i) {
				std::string::value_type c = (*i);

				// Keep alphanumeric and other accepted characters intact
				if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
					escaped << c;
					continue;
				}

				// Any other characters are percent-encoded
				escaped << '%' << std::setw(2) << int((unsigned char) c);
			}

			return escaped.str();
		}
		
		
	protected:
		const int m_protocol;
		const std::string m_role;
		std::set<int> m_messages_handled;
		
		/// ETP avro-encoding style (binary, json) for this session
        Etp::Encodings::values m_encoding;
		
	private:
	    sender_signal_t on_send;
		Energistics::Datatypes::SupportedProtocol _supported_protocol;
	};

}

/*
namespace avro {
	template<class T> struct codec_traits<Etp::etp_msg_ptr<T> > {	
		static void encode(Encoder& e, const Etp::etp_msg_ptr<T> v) {		
			avro::encode(e, *(v.get()));
		}		
		static void decode(Decoder& e, Etp::etp_msg_ptr<T> v) {		
			avro::decode(e, *(v.get()));
		}		
	};	
}
*/
#endif /* IPROTOCOLHANDLER_HPP_ */
