/*
 *License notice
 *
 * Energistics copyright 2016-
 * Energistics Transfer Protocol
 *
 * All rights in the WITSML� Standard, the PRODML� Standard, and the RESQML� Standard, or
 * any portion thereof, shall remain with Energistics or its suppliers and shall remain
 * subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement.
 *
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License.
 *
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied.
 *
 * See the License for the specific language governing permissions and limitations under the
 * License.
 *
 * All rights reserved.
 *
 */

/*  gencpp_handlers.js - Generate protocol handlers for ETP protocols.
 *            - requires node.js
 *            - assumes the existence of .avpr file, which has been sorted
 *  			into dependency order (see genProtocol.js, in the folder
 *  			$ENERGISTICS_ROOT/energyml/protocols/etpv1/build).
 *
 */

var fs = require('fs');

// Command-line arguments
var argv = require('optimist').default({
    protocolFile: '../etp/src/Schemas/etp.avpr',
    outputFile: './EtpMessageHandlers.hpp'
}).argv;

if(argv.help) {
    require('optimist').showHelp();
    process.exit(0);
}

var protocol = fs.readFileSync(argv.protocolFile, "ascii");
var schemas = JSON.parse(protocol).types;

// Angled includes
var stdIncludes = [];

// Quoted includes
var libIncludes = ["EtpErrorCodes.hpp", "BaseHandler.hpp"]


var ClassMaker = function () {
};

ClassMaker.prototype = {
    buffer: "",
	baseClass: "BaseHandler",
    indent: 0,
	indentString: "    ",
	unionCount: 0,
	signalPrefix: "Sig",
	anonymousUnions: [],
    definedTypes: {},

    cppName: function(name) {
        return name.replace(/\./g, "::");
    },

	newFile: function() {
		this.buffer = "";
	},

// The next batch of methods are just helpers to create a pretty-printed output.

    write: function (token) {
        for (var i = 0; i < this.indent; i++) {
            this.buffer += this.indentString;
        }
        this.buffer += token;
    },

    writeBlock: function (text) {
        var lines = text.split("\n");
        for (var line = 0; line < lines.length; line++) {
            this.write(lines[line]);
        }
    },

    start: function (token) {
        this.write(token);
        this.write("\n");
        this.indent++;
    },

    end: function (token) {
        this.indent--;
		if(token) {
			this.write(token);
			this.write("\n");
		}
    },

    beginNamespace: function (name) {
        var parts = name.split(".");
        for (var i = 0; i < parts.length; i++)
            this.line("namespace " + parts[i] + " {");
    },

    endNamespace: function(name) {
        var parts = name.split(".");
        for (var i = 0; i < parts.length; i++)
            this.line("}");
    },

	beginTraits: function(typeName) {
		this.start("namespace avro {");
		this.start("template<> struct codec_traits<" + typeName +"> {");
	},

	endTraits: function() {
		this.end("};");
		this.end("}");
	},

	beginEncoderTraits: function(typeName) {
		this.start("static void encode(Encoder& e, const " + typeName +"& v) {");
	},

	endEncoderTraits: function() {
		this.end("}");
	},

	beginDecoderTraits: function(typeName) {
		this.start("static void decode(Decoder& e, " + typeName +"& v) {");
	},

	endDecoderTraits: function() {
		this.end("}");
	},

    line: function (token) {
        this.write(token + "\n");
    },

    typeOf: function (value) {
        var s = typeof value;
        if (s === 'object') {
            if (value) {
                if (value instanceof Array) {
                    s = 'array';
                }
                else {
                }
            } else {
                s = 'null';
            }
        }
        return s;
    },

	generateSignal: function(definition, messageName) {
		var signalName = this.signalPrefix + messageName;
		this.line("etp_signal_t<" + messageName + "> " + signalName + ";");
	},

	generateSignals: function(definition) {
		for(var signal in definition.events) {
			if(definition.events.hasOwnProperty(signal)){
				this.generateSignal(definition, signal);
			}
		}
	},

	generateConnector: function(definition, messageName) {
		var signalName = this.signalPrefix + messageName;
		this.line("/// connect to the " + messageName + " signal");
		this.line("etp_sig_conn_t add_" + messageName + "_handler(const etp_slot_t<" + messageName + ">  &subscriber) { return " + signalName + ".connect(subscriber); }"
		);
	},

	generateConnectors: function(definition) {
		this.line("");
		this.line("// Event connectors");
		for(var signal in definition.events) {
			if(definition.events.hasOwnProperty(signal)){
				this.generateConnector(definition, signal);
			}
		}
	},

	generateSender: function(definition, messageName) {
		var callName = "send_" + messageName;
		this.line("/// Send the " + messageName + " on the websocket");
		this.line("int64_t " + callName + "(" + messageName + "Ptr  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }");
		this.line("int64_t " + callName + "(" + messageName + "&  message, int64_t correlationId=0, int32_t flags=0) { return send(message, correlationId, flags); }");
	},

	generateSenders: function(definition) {
		this.line("");
		this.line("// Message senders");
		for(var sender in definition.senders) {
			if(definition.senders.hasOwnProperty(sender)){
				this.generateSender(definition, sender);
			}
		}
	},

	generateMessageLoop: function(definition) {
		this.line("");
		this.line("/// Handle websocket messages for protocol: " + definition.protocolId + " (" + definition.namespace.split(".").pop() + "), role: " + definition.name.toLowerCase());
		this.start("void handle_message(MessageHeader header, avro::DecoderPtr decoder) override {");
			this.line("switch (header.m_messageType) {");
				for(var signal in definition.events) {
					if(definition.events.hasOwnProperty(signal)){

						this.start("case " + definition.events[signal].messageType +  " :");
						this.line(this.signalPrefix+signal+"(header, decode<" + signal + ">(decoder));");
						this.line("break;");
						this.end();
					}
				}

				this.start("default:");
				this.line("BaseHandler::handle_message(header, decoder);");
				this.line("break;");
				this.end();
			this.line("}");
		this.end("}\n");

	},

	generateConstructors: function(definition) {
		this.line(definition.name +"()");
		this.line("    : " + this.baseClass + "(" + definition.protocolId + ', "' + definition.name.toLowerCase() + '")');
		this.start('{');
		for(var signal in definition.events) {
			if(definition.events.hasOwnProperty(signal)) {
				this.line("m_messages_handled.insert(" + definition.events[signal].messageType +  ");");
			}
		}
		this.end('}');
		this.line("virtual ~" + definition.name +"() {}");
		// this.line("virtual BaseHandler* allocate() override { return new " + definition.name +"(); }");
	},

	generateInterface: function(definition)
	{
		this.line("");

		this.beginNamespace(definition.namespace);

		this.start("");
		this.line("/// Basic handler for protocol: " + definition.protocolId + " (" + definition.namespace.split(".").pop() + "), role: " + definition.name.toLowerCase());
		this.line("class " + definition.name + " : public " + this.baseClass);
		this.line("{");

		this.start("public:");
		this.generateConstructors(definition);
		this.generateMessageLoop(definition);
		this.generateConnectors(definition);
		this.generateSenders(definition);
		this.end("");

		this.start("public:");
		this.generateSignals(definition);
		this.end("");

		this.line("};")
		this.end("");

		this.endNamespace(definition.namespace);
	},

	unqualifiedName: function(name) {
		return (""+name).split(".").pop();
	},


	findProtocolRoles: function(s) {
		return this.namespace.replace('Energistics', 'Etp') + "." + s[0].toUpperCase()+s.substr(1);
	},

	findProtocols: function(schemas) {
		console.log("Preprocessing to find protocols");
		for (var i = 0; i < schemas.length; i++) {
			var thisRecord = schemas[i];
			// This is a top-level message.
			if(thisRecord.protocolRoles) {
				var roles = thisRecord.protocolRoles.split(",").map(this.findProtocolRoles, thisRecord);
				this.definedTypes[roles[0]]={events:{}, senders:{}};
				this.definedTypes[roles[1]]={events:{}, senders:{}};
			}
        }
        console.log("Assigning messages to interfaces");
		for (var i = 0; i < schemas.length; i++) {
			var thisRecord = schemas[i];
			// This is a top-level message.
			if(thisRecord.protocolRoles) {
				var interfaces = thisRecord.protocolRoles.split(",").map(this.findProtocolRoles, thisRecord);
				var roles = thisRecord.protocolRoles.split(",");

				this.definedTypes[interfaces[0]].protocolId=thisRecord.protocol;
				this.definedTypes[interfaces[1]].protocolId=thisRecord.protocol;
				this.definedTypes[interfaces[0]].name=roles[0][0].toUpperCase()+roles[0].substr(1);
				this.definedTypes[interfaces[1]].name=roles[1][0].toUpperCase()+roles[1].substr(1);
				this.definedTypes[interfaces[0]].namespace=thisRecord.namespace;
				this.definedTypes[interfaces[1]].namespace=thisRecord.namespace;

				// If a shared message (like Core::CloseSession), then create events and senders on both interfaces.
				if ((thisRecord.senderRole == "*") || (thisRecord.senderRole==thisRecord.protocolRoles)) {
					this.definedTypes[interfaces[0]]["senders"][thisRecord.name]=thisRecord;
					this.definedTypes[interfaces[0]]["events"][thisRecord.name]=thisRecord;
					this.definedTypes[interfaces[1]]["senders"][thisRecord.name]=thisRecord;
					this.definedTypes[interfaces[1]]["events"][thisRecord.name]=thisRecord;
				}
				// All others go to either sender and receiver
				else {
					this.definedTypes[interfaces[0]][((roles[1]==thisRecord.senderRole)?"events":"senders")][thisRecord.name]=thisRecord;
					this.definedTypes[interfaces[1]][((roles[0]==thisRecord.senderRole)?"events":"senders")][thisRecord.name]=thisRecord;
				}
			}
        }
        console.dir(this.definedTypes);

		return this;
	},

    createClasses: function (schemas) {
		console.log("Processing schema list");

		this.newFile();
		this.line("#ifndef "+ "ETP_MESSAGE_HANDLERS_HPP_");
		this.line("#define "+ "ETP_MESSAGE_HANDLERS_HPP_");
		// this.line("");

		stdIncludes.map(function(st){ this.line("#include <"+st+">");}.bind(this));
		this.line("");

		libIncludes.map(function(st){ this.line('#include "'+st+'"');}.bind(this));
		this.line("");

		this.line("using namespace Etp;");

		for (var type in this.definedTypes){
			if (this.definedTypes.hasOwnProperty(type)) {
				thisType = this.definedTypes[type];
				this.generateInterface(thisType);
			}
		}
		this.line("");
		this.line("#endif    // " + "ETP_MESSAGE_HANDLERS_HPP_");

		this.definitions = this.buffer;
    }
}

var classes = new ClassMaker();
classes.findProtocols(schemas).createClasses(schemas);
fdw = fs.openSync(argv.outputFile, 'w');
fs.writeSync(fdw, classes.definitions);
fs.closeSync(fdw);

process.exit(0);
