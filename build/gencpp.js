/*
 *License notice
 *  
 * Energistics copyright 2016-
 * Energistics Transfer Protocol
 *
 * All rights in the WITSML� Standard, the PRODML� Standard, and the RESQML� Standard, or
 * any portion thereof, shall remain with Energistics or its suppliers and shall remain 
 * subject to the terms of the Product License Agreement available at
 * http://www.energistics.org/product-license-agreement. 
 * 
 * Apache
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. 
 * 
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. 
 * 
 * See the License for the specific language governing permissions and limitations under the
 * License.
 * 
 * All rights reserved. 
 * 
 */

/*  gencpp.js - Generate simple c++ proxies for ETP Avro schemas.
 *            - requires node.js
 *            - assumes the existence of .avpr file, which has been sorted
 *  			into dependency order (see genProtocol.js, in the folder
 *  			energyml/protocols/etpv1/build).
 *
 *  Assumption is that there are no anonymous types except unions. In a pre-
 *  processing step, unions are decorated with unique types names and the
 *  fields containing the unions are converted to use this unique name. Unions
 *  still are of type array at base, and so are recognized as such and we
 *  generate a custom class and traits for them.
 *
 *  General strategy is the same as avrogencpp: for each unique ETP schema,
 *  generate a simple structure to hold the data, and serialzer trait functions
 *  in the avro namespace.
 *
 *  Unlike avrogencpp, schemas are nested into namespaces according to avro schema's
 *  fully qualified name.
 */

var fs = require('fs');

// Command-line arguments
var argv = require('optimist').default({
    protocolFile: 'etp/src/Schemas/etp.avpr',
    outputFile: "./EtpMessages.hpp"
}).argv;

if(argv.help) {
    require('optimist').showHelp();
    process.exit(0);
}

var protocol = fs.readFileSync(argv.protocolFile, "ascii");
var schemas = JSON.parse(protocol).types;

// Angle bracket includes
var stdIncludes = ["map", "vector", "string", "sstream", "iostream"];

// Quoted includes
var libIncludes = ["boost/any.hpp", "avro/Specific.hh", "avro/Encoder.hh", "avro/Decoder.hh", "EtpErrorCodes.hpp"]

var sharedPtr = "boost::shared_ptr"

var ClassMaker = function () {
};

ClassMaker.prototype = {
    buffer: "",
    indent: 0,
	unionCount: 0,
	anonymousUnions: [],
    definedTypes: {},

    cppName: function(name) {
		switch(name) {
            case "null":
            case "int":
				return "int32_t";
            case "long":
				return "int64_t"
            case "float":
				return "float";
			case "double":
				return "double";
            case "string":
            case "bytes":
                return "std::string";
            case "boolean":
				return "bool";
		}
        return name.replace(/\./g, "::");
    },

// The next batch of methods are just helpers to create a pretty-printed output.

    write: function (token) {
        for (var i = 0; i < this.indent; i++) {
            this.buffer += '\t';
        }
        this.buffer += token;
    },

    writeBlock: function (text) {
        var lines = text.split("\n");
        for (var line = 0; line < lines.length; line++) {
            this.write(lines[line]);
        }
    },

    start: function (token) {
        this.write(token);
        this.write("\n");
        this.indent++;
    },

    end: function (token) {
        this.indent--;
        this.write(token);
        this.write("\n");
    },

    beginNamespace: function (name) {
        var parts = name.split(".");
        parts.pop();
        for (var i = 0; i < parts.length; i++)
            this.start("namespace " + parts[i] + " {");
    },

    endNamespace: function(name) {
        var parts = name.split(".");
        parts.pop();
        for (var i = 0; i < parts.length; i++)
            this.end("};");
    },

	beginTraits: function(typeName) {
		this.start("namespace avro {");
		this.start("template<> struct codec_traits<" + typeName +"> {");
	},

	endTraits: function() {
		this.end("};");
		this.end("}");
	},

	beginEncoderTraits: function(typeName) {
		this.start("static void encode(Encoder& e, const " + typeName +"& v) {");
	},

	endEncoderTraits: function() {
		this.end("}");
	},

	beginDecoderTraits: function(typeName) {
		this.start("static void decode(Decoder& e, " + typeName +"& v) {");
	},

	endDecoderTraits: function() {
		this.end("}");
	},

    line: function (token) {
        this.write(token + "\n");
    },

    typeOf: function (value) {
        var s = typeof value;
        if (s === 'object') {
            if (value) {
                if (value instanceof Array) {
                    s = 'array';
                }
                else {
                }
            } else {
                s = 'null';
            }
        }
        return s;
    },

	generateGetterAndSetter: function(type, name,  idx)
	{
		// Getter
		this.start(type + "& get_" + name + "()  {");
        this.start( "if (idx_ != " + idx + ") {");
        this.write('throw avro::Exception("Invalid type for union.");\n');
        this.end("}");
        this.write("return boost::any_cast< " + type + "& >(value_);\n");
        this.end("}");

		// Setter
        this.start("void set_" + name + "(const " + type + "& v) {");
        this.write("idx_ = " + idx + ";\n");
        this.write("value_ = v;\n");
        this.end("}");
	},

	unqualifiedName: function(name) {
		return (""+name).split(".").pop();
	},

    writeUnion: function (name, types) {
		this.write("struct " + this.unqualifiedName(name) + " {\n");
		this.start("private:");
		this.write("size_t idx_=0;\n");
		this.write("boost::any value_;\n");
		this.end("");
		this.start("public:");
		this.write("size_t idx() const { return idx_; }\n");
        for (var i = 0; i < types.length; ++i) {
            var T = types[i];
            if (T == "null") {
				this.write("bool is_null() const { return idx_==" + i + "; }\n");
				this.write("void set_null() { idx_=" + i +"; value_ = boost::any(); }\n");
            }
            else if (typeof T == "object") {
                //constructors += "\n" + name + "(vector<" + this.cppName(T.type.items) + "> __object):__buffer(){ stringstream __sb; AvroEncode(__object, __sb); __buffer=__sb.str(); //m_position=" + i + ";}"
            }
            else {
				this.generateGetterAndSetter(this.cppName(T), this.unqualifiedName(T), i);
            }
        }
		//this.end("");
		this.end("};");
    },

	generateUnionTraits: function(name, types)
	{
		this.start("namespace avro {");

		this.start("template<> struct codec_traits<" + name + "> {\n");
			this.start("static void encode(Encoder& e, " + name + " v) {\n");
				this.write("e.encodeUnionIndex(v.idx());\n");
				this.start("switch (v.idx()) {");
					for (var i = 0; i < types.length; i++) {
						var nn = types[i];
						this.start("case " + i + ":");
							if (nn == "null") {
								this.write("e.encodeNull();\n");
							} else {
								this.write("avro::encode(e, v.get_" + this.unqualifiedName(nn) + "());\n");
							}
							this.write("break;");
						this.end("");
					}

				this.end("}");
			this.end("}");
			this.start("static void decode(Decoder& d, " + name + "& v) {\n");
				this.write("size_t n = d.decodeUnionIndex();\n")
				this.write('if (n >= ' + types.length + ') { throw avro::Exception("Union index too big"); }\n');
				this.start("switch (n) {");
					for (var i = 0; i < types.length; i++) {
						nn = types[i];
						this.start("case " + i + ":");
							this.start("{");
							if (nn == "null") {
								this.write("d.decodeNull();\n");
								this.write("v.set_null();\n");
							} else {
								this.write(this.cppName(nn) + " vv;\n");
								this.write("avro::decode(d, vv);\n");
								this.write("v.set_" + this.unqualifiedName(nn) + "(vv);\n");
							}
							this.end("}");
							this.write("break;");
						this.end("");
					}

				this.end("}");
			this.end("}");
		this.end("};");
		this.end("}\n");
	},

    writeType: function (schema, name) {
        var type;
        var i;
        var result;
        type = this.typeOf(schema);
        switch (type) {
            case "object":
                type = schema.type;
                break;
            case "string":
                type = schema;
                break;
            case "array":
                type = "union";
                break;
            default:
                throw "R:Invalid schema type: " + type + "in writeType()";
        }


        switch (type) {
            // Primitive types
            case "null":
                return;
            case "boolean":
                this.line("bool m_" + name + "=false;");
                return;

            case "int":
                this.line("int32_t m_" + name + ";");
                return;

            case "long":
                this.line("int64_t m_" + name + ";");
                return;

            case "float":
            case "double":
                this.line(type + " m_" + name + ";");
                return;

            case "bytes":
            case "string":
                this.line("std::string" + " m_" + name + ";");
                return;

                // Complex types
            case "record":
                if (this.definedTypes[name] === undefined) {
                    this.definedTypes[name] = schema;
                    this.beginNamespace(name);
					var className = name.split(".").pop();
                    this.start("struct " + className + "{");
                    for (i = 0; i < schema.fields.length; i++) {
                        console.dir(schema.fields[i])
                        this.writeType(schema.fields[i], ((schema.fields[i].type instanceof Array) ? schema.fields[i].name : schema.fields[i].name));
                    }
                    if(schema.messageType) {
                        this.line("static const int messageTypeId=" +  schema.messageType + ";");
                    }
                    this.end("};");
                    this.line("typedef " + sharedPtr + "<" +  className + "> " + className + "Ptr;");
                    if(schema.messageType) {
                        this.line("const int " + name.split(".").pop().toUpperCase() + "="+ schema.messageType + ";");
                    }
                    this.endNamespace(name);

					var theName = this.cppName(schema.fullName);
					this.beginTraits(theName);

					this.beginEncoderTraits(theName);
                    for (i = 0; i < schema.fields.length; i++) {
                        this.line("avro::encode(e, v.m_" + schema.fields[i].name + ");");
                    }
					this.endEncoderTraits();

					this.beginDecoderTraits(theName);
                    for (i = 0; i < schema.fields.length; i++) {
                        this.line("avro::decode(e, v.m_" + schema.fields[i].name + ");");
                    }
					this.endDecoderTraits();

                    this.endTraits();
                }
                else {
                    this.line(name + " m_" + name + ";");
                }


                return;

            case "enum": {
                console.log("Registering Enum:" + name);
                this.definedTypes[name] = schema;
                var enumIdx;
                this.beginNamespace(name);
                this.start("enum " + schema.name + " {");
                for (enumIdx = 0; enumIdx < schema.symbols.length; enumIdx++) {
                    this.line(schema.symbols[enumIdx] + "=" + enumIdx + ((enumIdx == (schema.symbols.length - 1)) ? "" : ","));
                }
                this.end("};");
                this.endNamespace(name);
				// Traits
				var theName = this.cppName(schema.fullName);
				this.beginTraits(theName);
				this.beginEncoderTraits(theName);
				this.line("e.encodeEnum(v);");
				this.endEncoderTraits();

				//this.beginDecoderTraits(theName);
				this.start("static void decode(Decoder& e, " + theName +"& v) {");
				this.line("v = static_cast<"+theName+">(e.decodeEnum());");
				this.endDecoderTraits();
				this.endTraits();
            }
                return;

            case "array":
                this.line("std::vector<" + this.cppName(schema.items) + "> m_" + name + ";");
                return;

            case "map":
                this.line("std::map<std::string, " + this.cppName(schema.values) + "> m_" + name + ";");
                return result;

            case "union":

				console.log("union: " + schema.fullName);
                if (this.definedTypes[name] === undefined) {
                    this.definedTypes[name] = schema;
                    this.beginNamespace(name);
					this.writeUnion(name, schema);
                    this.endNamespace(name);

					var theName = this.cppName(schema.fullName);
					this.generateUnionTraits(theName, schema);
                }
                else {
                    this.line(name + " m_" + name + ";");
                }

                return;

            default:
                if (this.definedTypes[type] !== undefined) {
                    this.line(this.cppName(type) + " m_" + name + ";");
                }
                else {
                    this.writeType(schema.type, name);
                }
        }
    },

	findUnions: function(schemas) {
		console.log("Preprocessing anonymous unions");
		for (var i = 0; i < schemas.length; i++) {
			var thisRecord = schemas[i];
			if(thisRecord.fields) {
				for(var j = 0; j<thisRecord.fields.length; j++) {
					var thisField = thisRecord.fields[j];
					if (this.typeOf(thisField.type)=="array") {
						// Turn the anonymous union into a first class type
						// name metadata.
						var union = thisField.type;
						union.fullName = thisRecord.fullName + thisField.name + "_t";

						// Now the union will not be anonymous, we can use it's fully qualified
						// name for this field.
						thisField.type = union.fullName;
						console.warn("union: " + union.fullName);
						schemas.splice(i, 0, union);
						i++;
					}
				}
			}
        }

		return this;
	},

    createClasses: function (schemas) {
		console.log("Processing schema list");

		stdIncludes.map(function(st){ this.line("#include <"+st+">");}.bind(this));
		this.line("");

		libIncludes.map(function(st){ this.line('#include "'+st+'"');}.bind(this));
		this.line("");

		for (var i = 0; i < schemas.length; i++) {
            console.log("Generating: " + schemas[i].fullName);
            this.writeType(schemas[i], schemas[i].fullName);
        }

		this.definitions = this.buffer;
    }
}


function isComplex(name) {
    return ['boolean', 'int', 'long', 'double', 'float', 'string', 'fixed', 'bytes', 'null', 'enum'].indexOf(name) < 0;
}

function typeList(schema) {
    switch (typeof schema) {
        case "string":
            if (isComplex(schema))
                this.depends.push(schema);
            break;
        case "object":
            if (schema.fields)
                schema.fields.map(typeList, this);
            else if (schema instanceof Array)
                schema.map(typeList, this);
            else if (schema.type == 'array')
                typeList.bind(this)(schema.items);
            else if (schema.type == 'map')
                typeList.bind(this)(schema.values);
            else if (schema.type)
                typeList.bind(this)(schema.type);
            else
                throw ("Unknown schema type in typeList()");
            break;
    }

    return this;
}

var classes = new ClassMaker();
classes.findUnions(schemas).createClasses(schemas);

fdw = fs.openSync(argv.outputFile, 'w');
fs.writeSync(fdw, "#ifndef ETP_MESSAGES__\n#define ETP_MESSAGES__\n\n");
fs.writeSync(fdw, classes.definitions);
fs.writeSync(fdw, "\n\n#endif");
fs.closeSync(fdw);

process.exit(0);
