# etp-cpp #

This package provides basic proxy class generation for implementing Energistics Transfer Protocol (ETP) in c++.

## Prerequisites ##
1. CMake >= 2.8
2. Boost libraries >= 1.54
3. Avro c++ libraries
4. websocketpp
5. etp-cpp message and protocol definitions.

## Setup ##
1. Clone the repository
2. Create a build directory. We recommend out of source builds.
3. Run CMake, adjust as necessary to find boost, etc.
4. Run make install.